
# Netmonk - Deployment Script
This repository contains free-version NetMonk's deployment script

## How to Install
```bash
$ curl -sSL get.netmonk.ketitik.com | sh
```


## How to Uninstall
```
$ curl -sSL remove.netmonk.ketitik.com | sh
```