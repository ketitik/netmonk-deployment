print("Begin Init Script.")
db = db.getSiblingDB('nm_db');
db.users.insert({
    uuid : "b8f239e1-0de0-45c9-8892-d8ed55cec341",
    username : "root",
    password : "10/w7o2juYBrGMh32/KbveULW9jk2tejpyUAD+uC6PE=",
    roles : "root",
    joined_at : ISODate("2018-06-26T09:44:12.242Z")
});
db.probes_registrant.insert({
    _id : ObjectId("59e6edcec79b098237220485"),
    probe_name : "default.netmonk.probe",
    location : "netmonk default",
    remote_ip : "0.0.0.0",
    key : "12345zxcvb",
    snmp_version : 2,
    scarpe_interval : NumberInt("300"),
    snmp_community : "public",
    snmp_username : "1",
    snmp_password : "2",
    security_level : "1",
    auth_protocol : "2",
    priv_protocol : "1",
    priv_password : "2aa",
    updated_at : ISODate("2017-10-26T05:39:25.796Z")
});
db.node_inventory.insert({
    _id : ObjectId("5b556d362305a34dee3bda98"),
    node_name : "example node 1",
    ip_mgmt : "10.100.10.1",
    probe_id : ObjectId("59e6edcec79b098237220485"),
    probe_name : "",
    tags : [""],
    notes : [""],
    username : "",
    password : "",
    created_at : Date(-62135596800000),
    updated_at : ISODate("2018-08-15T08:11:50.586Z")
});
db.node_inventory.insert({
    _id : ObjectId("5b5579682305a34dee3bda99"),
    node_name : "example node 2",
    ip_mgmt : "10.100.10.2",
    probe_id : ObjectId("59e6edcec79b098237220485"),
    probe_name : "",
    tags : [""],
    notes : [""],
    username : "",
    password : "",
    created_at : Date(-62135596800000),
    updated_at : ISODate("2018-08-15T08:11:50.586Z")
})
db.createUser({
    user: 'nmuser247',
   pwd: 'nmpass247',
   roles: [{ role: 'readWrite', db:'nm_db'}]
})
print("DONE.")
