#!/bin/sh
cd db
./up.sh

cd ../agent
./up.sh

cd ../kong
./up.sh

cd ../backend
./up.sh

cd ../few
./up.sh

cd ../chronus
./up.sh